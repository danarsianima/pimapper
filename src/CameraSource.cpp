#include "CameraSource.h"

CameraSource::CameraSource(){
	name = "Camera Source";
	cameraWidth = 1280;
	cameraHeight = 720;
	allocate(cameraWidth, cameraHeight);

	settings.sensorWidth = cameraWidth;
	settings.sensorHeight = cameraHeight;
	settings.enableTexture = true;
	videoGrabber.setup(settings);
}

void CameraSource::draw(){
	ofClear(0);
	ofSetHexColor(0xffffff);
	
	ofDisableNormalizedTexCoords();
	videoGrabber.draw(0, 0);
	ofEnableNormalizedTexCoords();
}
